#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import datetime
import logging
from collections import OrderedDict
import os
import pygal
import re
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from api import api
from config import config
from flask import Flask, render_template, request, jsonify, flash, redirect, url_for
from flask_mail import Message, Mail
from forms import ContactForm
from logging.handlers import RotatingFileHandler
from utils import database, geo, app_path, cache, price_calculator, daytime, zip_code
from pygal.style import *

# Logging
logger = logging.getLogger("logger")
logger.setLevel(logging.DEBUG)
if not os.path.isdir(os.path.join(app_path.get_app_path(), 'log')):
    os.mkdir(os.path.join(app_path.get_app_path(), 'log'))
handler = RotatingFileHandler(os.path.join(app_path.get_app_path(), 'log', 'tanken.log'),
                              10485760, 10)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s [%(module)-12.12s] [%(levelname)-5.5s]  %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
streamHandler = logging.StreamHandler()
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

app = Flask(__name__)

# Sets the app secret key from config
app.secret_key = config.FLASK_SECRET_KEY

# Configure the mailing for the contact form
mail = Mail()
app.config["MAIL_SERVER"] = config.MAIL_SERVER
app.config["MAIL_PORT"] = config.MAIL_PORT
app.config["MAIL_USE_SSL"] = config.MAIL_USE_SSL
app.config["MAIL_USERNAME"] = config.MAIL_USERNAME
app.config["MAIL_PASSWORD"] = config.MAIL_PASSWORD
mail.init_app(app)

# Google Analytics key
app.config['GANALYTICS_KEY'] = config.GANALYTICS_KEY


# Displays the index page
@app.route('/')
def main():
    return redirect(url_for('comparison'))
    # return render_template('index.html')


# Displays the comparison page
@app.route('/comparison/', methods=['GET', 'POST'])
def comparison():
    """
    :return: Returns the rendered form template if the request method is GET
    :return: Returns the rendered compraision result template if all data where correct.
    """
    # TODO Check if the country is germany
    # TODO BUG: If the users uses his GPS and then types in an address manually
    # TODO BUG: the users lat&lng will be used for google maps coords instead of the new.
    # TODO Improve the design for the form
    if request.method == "GET":
        return render_template('comparison.html')
    elif request.method == "POST":
        # If no address is provided return the template with an error
        if not request.form['address']:
            return render_template('comparison.html',
                                   msg="Bitte geben Sie eine Adresse an oder lassen Sie ihren Standort ermitteln.")
        # Get the values from the form
        address = request.form['address']
        rnge = request.form['range']
        gas_type = request.form['type']
        opened = request.form.getlist('opened')
        sort = request.form['sort']
        a = api.Api()
        try:
            if request.form['lat'] and request.form['lng']:
                lat = request.form['lat']
                lng = request.form['lng']
            else:
                pass
            # If the provided address is a zip code request the list of stations with the zip code
            # If not, request a list from the exact address from the api
            if zip_code.is_german_zip(address):
                logger.debug("User provided a zip code: {}".format(address))
                stations = a.get_list(rad=rnge, sort="dist", g_type=gas_type, zip_code=address)['stations']
                try:
                    lat, lng = geo.get_coords_from_zip(zip_code=address)
                except TypeError:
                    return render_template("comparison.html",
                                           msg="Es konnte keine Adresse aus Ihren Angaben ermittelt werden.")
            else:
                logger.debug("User provided a address code: {}".format(address))
                stations = a.get_list(rad=rnge, sort="dist", g_type=gas_type, addr=address)['stations']
                if stations is "API down":
                    return render_template("comparison.html",
                                           msg="Leider ist die externe API derzeit nicht verfügbar. "
                                               "Bitte versuchen Sie es später erneut.")
                try:
                    lat, lng = geo.get_coords_from_address(address=address)
                except TypeError:
                    return render_template("comparison.html",
                                           msg="Es konnte keine Adresse aus Ihren Angaben ermittelt werden.")
        except Exception as e:
            logger.exception(e)
            return render_template("comparison.html", msg="Es konnte keine Adresse aus Ihren Angaben ermittelt werden.")
        # Generates a list of all stations. If the user wants only opened stations they get sorted out here
        found_stations = []
        for station in stations:
            if "opened" in opened:
                if station['isOpen']:
                    found_stations.append(station)
            else:
                found_stations.append(station)

        # If the user don't want to see all prices he can choose the sorting method
        # TODO Only single price lists can get sorted atm. Provide a way to sort everything
        if "all" not in gas_type and "price" in sort:
            found_stations = sorted(found_stations, key=lambda k: k['price'], reverse=False)
        # else:
        #    found_stations = sorted(found_stations, key=lambda k: k['diesel'], reverse=False)
        #    found_stations = sorted(found_stations, key=lambda k: k['e10'], reverse=False)
        #    found_stations = sorted(found_stations, key=lambda k: k['e5'], reverse=False)
        return render_template('comparison_result.html', address=address, range=rnge, type=gas_type,
                               found_stations=found_stations, lat=lat, lng=lng)


# Displays the details page
@app.route('/detail/')
def detail():
    """
    :return: A rendered detail template of the desired gas station
    """
    # Station ID validation
    s_id = request.args.get('id')
    s_id_re = re.compile("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}")
    if s_id_re.match(s_id):
        logger.debug("Station ID is valid {}".format(s_id))
    else:
        logger.debug("Station ID is NOT valid {}".format(s_id))
        msg = "Your station ID is not valid"
        return render_template('error.html', msg=msg)

    a = api.Api()
    c = cache.Cache()
    d = database.Database()
    lat = request.args.get('lat')
    lng = request.args.get('lng')

    # TODO Move caching to api/api.py class
    try:
        details = c.get_cached(s_id=s_id)
        if not details:
            details = a.get_detail(s_id=s_id)['station']
            # add the history data only if the details are not from cache
            d.add_history(data=details)
            c.set_cached(details)
    except Exception as e:
        logger.exception(e)
        msg = "API error or no station found for the provided ID."
        return render_template("error.html", msg=msg)

    history = d.get_history(s_id=s_id)

    diesel_prices = OrderedDict()
    e5_prices = OrderedDict()
    e10_prices = OrderedDict()

    diesel_prices_yesterday = {}
    e5_prices_yesterday = {}
    e10_prices_yesterday = {}

    diesel_prices_two_days_ago = {}
    e5_prices_two_days_ago = {}
    e10_prices_two_days_ago = {}

    # Add all prices and dates to lists
    for x in history:
        # Get 7 days prices
        if (datetime.datetime.now() - x['date']).days < 8:
            date = x['date'].strftime('%d.%m.%Y')
            if not date == datetime.date.today().strftime('%d.%m.%Y'):
                if date not in diesel_prices.keys():
                    if not x['diesel'] == 0.0 and x['diesel'] is not None:
                        diesel_prices[x['date'].strftime('%d.%m.%Y')] = float(x['diesel'] / 1000.0)
                if date not in e5_prices.keys():
                    if not x['e5'] == 0.0 and x['e5'] is not None:
                        e5_prices[x['date'].strftime('%d.%m.%Y')] = float(x['e5'] / 1000.0)
                if date not in e10_prices.keys():
                    if not x['e10'] == 0.0 and x['e10'] is not None:
                        e10_prices[x['date'].strftime('%d.%m.%Y')] = float(x['e10'] / 1000.0)

        # Get yesterdays prices
        if (datetime.datetime.now().date() - x['date'].date()).days == 1 \
                and datetime.datetime.now().date() != x['date'].date():
            if not x['diesel'] == 0.0 and x['diesel'] is not None:
                diesel_prices_yesterday[x['date']] = float(x['diesel'] / 1000.0)
            if not x['e5'] == 0.0 and x['e5'] is not None:
                e5_prices_yesterday[x['date']] = float(x['e5'] / 1000.0)
            if not x['e10'] == 0.0 and x['e10'] is not None:
                e10_prices_yesterday[x['date']] = float(x['e10'] / 1000.0)
        # Get the prices from two days ago
        if (datetime.datetime.now().date() - x['date'].date()).days == 2 \
                and datetime.datetime.now().date() != x['date'].date():
            if not x['diesel'] == 0.0 and x['diesel'] is not None:
                diesel_prices_two_days_ago[x['date']] = float(x['diesel'] / 1000.0)
            if not x['e5'] == 0.0 and x['e5'] is not None:
                e5_prices_two_days_ago[x['date']] = float(x['e5'] / 1000.0)
            if not x['e10'] == 0.0 and x['e10'] is not None:
                e10_prices_two_days_ago[x['date']] = float(x['e10'] / 1000.0)

    # calculate the cheapest daytime for refuelling
    cheapest_price_yesterday = {
        'diesel': price_calculator.cheapest(diesel_prices_yesterday),
        'e5': price_calculator.cheapest(e5_prices_yesterday),
        'e10': price_calculator.cheapest(e10_prices_yesterday)
    }

    cheapest_price_two_days_ago = {
        'diesel': price_calculator.cheapest(diesel_prices_two_days_ago),
        'e5': price_calculator.cheapest(e5_prices_two_days_ago),
        'e10': price_calculator.cheapest(e10_prices_two_days_ago)
    }

    cheapest_daytime_two_days_ago = {}
    for key, val in cheapest_price_two_days_ago.items():
        if not val[0]:
            cheapest_daytime_two_days_ago[key] = "unbekannt"
        else:
            cheapest_daytime_two_days_ago[key] = daytime.get_daytime(val[0])

    cheapest_daytime_yesterday = {}
    for key, val in cheapest_price_yesterday.items():
        if not val[0]:
            cheapest_daytime_yesterday[key] = "unbekannt"
        else:
            cheapest_daytime_yesterday[key] = daytime.get_daytime(val[0])

    # Append the current values to the lists
    diesel_prices[datetime.datetime.now().strftime('%d.%m.%Y')] = details['diesel']
    e5_prices[datetime.datetime.now().strftime('%d.%m.%Y')] = details['e5']
    e10_prices[datetime.datetime.now().strftime('%d.%m.%Y')] = details['e10']

    # Determine the average prices
    # Yesterdays avg pricing
    if None not in diesel_prices_yesterday and sum(diesel_prices_yesterday.values()) > 0:
        diesel_avg_price_yesterday = round(sum(diesel_prices_yesterday.values()) / len(diesel_prices_yesterday), 3)
    else:
        diesel_avg_price_yesterday = None
    if None not in e5_prices_yesterday and sum(e5_prices_yesterday.values()) > 0:
        e5_avg_price_yesterday = round(sum(e5_prices_yesterday.values()) / len(e5_prices_yesterday), 3)
    else:
        e5_avg_price_yesterday = None

    if None not in e10_prices_yesterday and sum(e10_prices_yesterday.values()) > 0:
        e10_avg_price_yesterday = round(sum(e10_prices_yesterday.values()) / len(e10_prices_yesterday), 3)
    else:
        e10_avg_price_yesterday = None
    # Two days ago avg pricing
    if None not in diesel_prices_two_days_ago and sum(diesel_prices_two_days_ago.values()) > 0:
        diesel_avg_prices_two_days_ago = round(
            sum(diesel_prices_two_days_ago.values()) / len(diesel_prices_two_days_ago), 3)
    else:
        diesel_avg_prices_two_days_ago = None
    if None not in e5_prices_two_days_ago and sum(e5_prices_two_days_ago.values()) > 0:
        e5_avg_prices_two_days_ago = round(sum(e5_prices_two_days_ago.values()) / len(e5_prices_two_days_ago), 3)
    else:
        e5_avg_prices_two_days_ago = None
    if None not in e10_prices_two_days_ago and sum(e10_prices_two_days_ago.values()) > 0:
        e10_avg_prices_two_days_ago = round(sum(e10_prices_two_days_ago.values()) / len(e10_prices_two_days_ago), 3)
    else:
        e10_avg_prices_two_days_ago = None

    # Last weeks avg pricing
    if None not in diesel_prices.values() and sum(diesel_prices.values()) > 0:
        diesel_avg_price = round(sum(diesel_prices.values()) / len(diesel_prices), 3)
    else:
        diesel_avg_price = 0
    diesel_now = details['diesel']
    if None not in e5_prices.values() and sum(e5_prices.values()) > 0:
        e5_avg_price = round(sum(e5_prices.values()) / len(e5_prices), 3)
    else:
        e5_avg_price = 0
    e5_now = details['e5']
    if None not in e10_prices.values() and sum(e10_prices.values()) > 0:
        e10_avg_price = round(sum(e10_prices.values()) / len(e10_prices), 3)
    else:
        e10_avg_price = 0
    e10_now = details['e10']

    # Generate Charts for diesel, e5 and e10
    diesel_chart_title = ""
    diesel_chart = pygal.Line(width=700, height=400, x_label_rotation=35,
                              explicit_size=True, title=diesel_chart_title,
                              disable_xml_declaration=True, fill=True, interpolate='cubic', style=DarkGreenBlueStyle)
    diesel_chart.x_labels = diesel_prices.keys()
    diesel_chart.add("Preis", diesel_prices.values())
    diesel_chart.value_formatter = lambda y: "€{:,.3f}".format(y)
    diesel_chart.config.dots_size = 4

    e5_chart_title = ""
    e5_chart = pygal.Line(width=700, height=400, x_label_rotation=35,
                          explicit_size=True, title=e5_chart_title,
                          disable_xml_declaration=True, fill=True, interpolate='cubic', style=DarkGreenBlueStyle)
    e5_chart.x_labels = e5_prices.keys()
    e5_chart.add(title="Preis", values=e5_prices.values())
    e5_chart.value_formatter = lambda y: "€{:,.3f}".format(y)
    e5_chart.config.dots_size = 4

    e10_chart_title = ""
    e10_chart = pygal.Line(width=700, height=400, x_label_rotation=35,
                           explicit_size=True, title=e10_chart_title,
                           disable_xml_declaration=True, fill=True, interpolate='cubic', style=DarkGreenBlueStyle)
    e10_chart.x_labels = e10_prices.keys()
    e10_chart.add(title="Preis", values=e10_prices.values())
    e10_chart.value_formatter = lambda y: "€{:,.3f}".format(y)
    e10_chart.config.dots_size = 4

    # Return the rendered template with all values
    return render_template('detail.html', details=details, lat=lat, lng=lng,
                           diesel_chart=diesel_chart,
                           e5_chart=e5_chart,
                           e10_chart=e10_chart,
                           diesel_avg_price=diesel_avg_price, diesel_now=diesel_now,
                           e5_avg_price=e5_avg_price, e5_now=e5_now,
                           e10_avg_price=e10_avg_price, e10_now=e10_now,
                           diesel_avg_price_yesterday=diesel_avg_price_yesterday,
                           e5_avg_price_yesterday=e5_avg_price_yesterday,
                           e10_avg_price_yesterday=e10_avg_price_yesterday,
                           diesel_avg_prices_two_days_ago=diesel_avg_prices_two_days_ago,
                           e5_avg_prices_two_days_ago=e5_avg_prices_two_days_ago,
                           e10_avg_prices_two_days_ago=e10_avg_prices_two_days_ago,
                           cheapest_daytime_yesterday=cheapest_daytime_yesterday,
                           cheapest_daytime_two_days_ago=cheapest_daytime_two_days_ago,
                           gmaps_api_key=config.GMAPS_API_KEY
                           )


# Determine the current address from lat/lng
@app.route('/geoloc')
def geoloc():
    """
    :return: The current address from lat/lng as json response
    """
    lat = request.args.get('lat')
    lng = request.args.get('lng')
    result = geo.get_address_from_coords(lat, lng)
    return jsonify(result=result)


# Displays the legal page
@app.route('/legal/')
def legal():
    return render_template('legal.html')


# Displays the about page
@app.route('/about/')
def about():
    return render_template('about.html')


# Displays the contact form
@app.route('/contact/', methods=['GET', 'POST'])
def contact():
    form = ContactForm()
    if request.method == 'POST':
        if not form.validate():
            # If the form is not filled out correctly return the error template
            flash('Alle Felder müssen ausgefüllt werden.')
            return render_template('contact.html', form=form)
        else:
            # If the form is valid, send the email and return a success message
            msg = Message("Kontaktformular", sender=config.MAIL_SENDER, recipients=config.MAIL_RECIPIENT)
            msg.body = "From: {} - {}\nMessage:\n{}".format(form.name.data, form.email.data, form.message.data)
            mail.send(msg)
            return render_template('contact.html', success=True)
    elif request.method == 'GET':
        return render_template('contact.html', form=form)


# Error handlers
@app.errorhandler(404)
def page_not_found(e):
    logger.warn(e)
    return render_template('http_errors/404.html'), 404


@app.errorhandler(500)
def page_not_found(e):
    logger.error(e)
    return render_template('http_errors/500.html'), 500


@app.errorhandler(403)
def page_not_found(e):
    logger.warn(e)
    return render_template('http_errors/403.html'), 403


@app.errorhandler(503)
def page_not_found(e):
    logger.error(e)
    return render_template('http_errors/503.html'), 503


# Filters
@app.template_filter('timestamp')
def format_datetime(value):
    time_obj = datetime.datetime.strptime(value, "%H:%M:%S")
    return time_obj.strftime("%H:%M")


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

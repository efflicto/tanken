#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import requests
import logging
from geopy.geocoders import Nominatim
from config import config
from utils import geo
from requests.packages.urllib3.exceptions import MaxRetryError, TimeoutError, ConnectTimeoutError, HTTPError

class Api:
    def __init__(self):
        self.logger = logging.getLogger("logger")

    def test_api(self):
        """
        Tests if the API available
        """
        response = requests.get(url=config.LIST_URL)
        if response.status_code == 200:
            return True
        else:
            self.logger.error("No valid response: {}".format(response.status_code))
            return False

    def get_list(self, rad, sort, g_type, lat=None, lng=None, zip_code=None, addr=None):
        """
        Searches for valid gas station with the give parameters.
        :param rad: Search radius
        :param sort: Sort by
        :param g_type: type 'price' or 'dist'
        :param lat: latitude
        :param lng: longitude
        :param zip_code: ZIP code
        :param addr: Address
        :return: json object of found gas stations
        """
        if zip_code:
            latitude, longitude = geo.get_coords_from_zip(zip_code=zip_code)
            if not latitude and longitude:
                self.logger.warn("No lat&lng from {}".format(zip_code))
                return False
        elif lat and lng:
            latitude = lat
            longitude = lng
        elif addr:
            latitude, longitude = geo.get_coords_from_address(address=addr)
            if not latitude and longitude:
                self.logger.warn("No lat&lng from {}".format(addr))
                return False
        params = {
            'lat': latitude,
            'lng': longitude,
            'rad': rad,
            'sort': sort,
            'type': g_type,
            'apikey': config.API_KEY
        }
        self.logger.debug("API request with: {}".format(params))
        if g_type is "all":
            params['sort'] = 'dist'
        try:
            response = requests.get(url=config.LIST_URL, params=params)
            if response.status_code is 200:
                return response.json()
            else:
                self.logger.error("No valid response: {}".format(response.status_code))
                return False
        except ConnectionError as e:
            #self.logger.exception(e)
            return "API down"
        except MaxRetryError as e:
            return "API down"
        except TimeoutError as e:
            return "API down"
        except HTTPError as e:
            return "API down"
        except WindowsError as e:
            return "API down"
        except Exception as e:
            self.logger.exception(e)
            return False

    def get_detail(self, s_id):
        """
        Gets details for a gas station with the given station ID
        :param s_id: a valid gas station ID
        :return: a json object with details
        """
        # TODO Move the caching logic from tanken.py/details() to this function
        params = {
            'id': s_id,
            'apikey': config.API_KEY
        }
        try:
            response = requests.get(url=config.DETAIL_URL, params=params)
            if response.status_code is 200:
                self.logger.debug("Details from API: {}".format(response.json()))
                return response.json()
            else:
                self.logger.error("No valid response: {}".format(response.status_code))
                return False
        except Exception as e:
            self.logger.exception(e)
            return False

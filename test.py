import datetime
import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from api import api
from utils import geo, history, database, cache, app_path, daytime
import logging
import re

from logging.handlers import RotatingFileHandler

logger = logging.getLogger("logger")
logger.setLevel(logging.DEBUG)
if not os.path.isdir(os.path.join(app_path.get_app_path(), 'log')):
    os.mkdir(os.path.join(app_path.get_app_path(), 'log'))
handler = RotatingFileHandler(os.path.join(app_path.get_app_path(), 'log', 'tanken.log'),
                              10485760, 10)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s [%(module)-12.12s] [%(levelname)-5.5s]  %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
streamHandler = logging.StreamHandler()
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

print(geo.get_coords_from_zip("0847"))
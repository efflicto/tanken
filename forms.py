#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from flask_wtf import Form
from wtforms import StringField, TextAreaField, SubmitField, validators


class ContactForm(Form):
    """
    Generates a Flask-WTF form for the contact form
    """
    name = StringField('Ihr Name', [validators.DataRequired(message="Bitte füllen Sie das Feld aus.")])
    email = StringField('Ihre E-Mail Adresse', [validators.DataRequired(message="Bitte füllen Sie das Feld aus."),
                                                validators.Email(message="Keine valide E-Mail Addresse")])
    message = TextAreaField('Ihre Nachricht:', [validators.DataRequired(message="Bitte füllen Sie das Feld aus.")])
    submit = SubmitField('Abschicken')

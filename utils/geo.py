#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging
import sys
import os

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from geopy.geocoders import Nominatim

logger = logging.getLogger("logger")


def get_address_from_coords(lat, lng):
    """
    Returns an address string based on latitude and longitude.
    If not valid address can be found, False is returned.
    :param lat: Latitude
    :param lng: Longitude
    :return: Adress if found from lat&long or False if not
    """
    try:
        geolocator = Nominatim()
        rev = geolocator.reverse("{}, {}".format(lat, lng))

        if 'house_number' in rev.raw['address']:
            address = "{} {}, {} {}, {}".format(rev.raw['address']['road'],
                                                rev.raw['address']['house_number'],
                                                rev.raw['address']['postcode'],
                                                rev.raw['address']['county'],
                                                rev.raw['address']['country'])
        else:
            address = "{}, {} {}, {}".format(rev.raw['address']['road'],
                                             rev.raw['address']['postcode'],
                                             rev.raw['address']['county'],
                                             rev.raw['address']['country'])
        logger.debug("Geolocation determined: {} from {}".format(address, lat + "|" + lng))
        return address
    except Exception as e:
        logger.exception(e)
        return False


def get_coords_from_address(address):
    """
    Returns Latitude and Longitude from a address
    :param address:
    :return: lat and lng
    """
    try:
        geolocator = Nominatim()
        location = geolocator.geocode(address)
        logger.debug(
            "[ADDR]Lat/Lng determined: {} from {}".format(str(location.latitude) + "|" + str(location.longitude), address))
        if "Deutschland" in location.raw['display_name']:
            return location.latitude, location.longitude
        else:
            logger.warn("No valid location found for {}".format(address))
            return False
    except Exception as e:
        logger.exception(e)
        return False


def get_coords_from_zip(zip_code):
    try:
        geo = Nominatim()
        location = geo.geocode("{} Deutschland".format(zip_code))
        logger.debug(location.raw)
        if "Deutschland" in location.raw['display_name']:
            logger.debug(
                "[ZIP]Lat/Lng determined: {} from {}".format(str(location.latitude) + "|" + str(location.longitude),
                                                        zip_code))
            return location.latitude, location.longitude
        else:
            logger.warn("No valid location found for {}".format(zip_code))
            return False
    except Exception as e:
        logger.exception(e)
        return False

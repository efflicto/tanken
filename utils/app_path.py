# -*- coding: utf-8 -*-
#!/usr/bin/env python3
import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


def get_app_path():
    """
    :return: The path of the application
    """
    return os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))

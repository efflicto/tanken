#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import re


def is_german_zip(zip_code):
    """
    :param zip_code:
    :return: True if ZIP code is valid
    """
    if zip_code.isdigit() and len(zip_code) == 5:
        plz_re = re.compile("((?:0[1-46-9]\d{3})|(?:[1-357-9]\d{4})|(?:[4][0-24-9]\d{3})|(?:[6][013-9]\d{3}))")
        if plz_re.match(zip_code):
            return True
        else:
            return False
    else:
        return False

#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def get_daytime(date_time):
    if 6 <= date_time.hour <= 10:
        return "morgens"
    elif 10 <= date_time.hour < 12:
        return "vormittags"
    elif 12 <= date_time.hour < 13:
        return "mittags"
    elif 13 < date_time.hour <= 15:
        return "nachmittags"
    elif 16 <= date_time.hour <= 22:
        return "abends"
    else:
        return "nachts"

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import dataset
import logging
import time
import datetime
import shutil
import gzip
import requests

from config import config
from utils import app_path


class History:
    def __init__(self):
        self.logger = logging.getLogger("history_logger")

    def download_history(self):
        """
        Downloads the history data for the configured url
        """
        self.logger.info("Downloading archive file.")
        if not os.path.isdir(os.path.join(app_path.get_app_path(), 'temp')):
            os.mkdir(os.path.join(app_path.get_app_path(), 'temp'))
        with open(os.path.join(app_path.get_app_path(), 'temp/history.gz'), 'wb') as handle:
            response = requests.get(config.HISTORY_URL, stream=True)
            if not response.ok:
                pass
            for block in response.iter_content(1024):
                handle.write(block)

    def re_generate_db(self, days):
        """
        Generates the history db within the given days and saves the user generated data (history=False)
        :param days:
        """
        # Create paths
        if not os.path.isdir(os.path.join(app_path.get_app_path(), "database")):
            os.mkdir(os.path.join(app_path.get_app_path(), "database"))
        if os.path.isfile(os.path.join(app_path.get_app_path(), "database/history_temp.db")):
            os.remove(os.path.join(app_path.get_app_path(), "database/history_temp.db"))

        # Open connections to the database files
        db = dataset.connect("sqlite:///"+os.path.join(app_path.get_app_path(), "database/history.db"))
        table = db['history_data']

        temp_db = dataset.connect("sqlite:///"+os.path.join(app_path.get_app_path(), "database/history_temp.db"))
        table_temp = temp_db['history_data']
        self.download_history()
        self.logger.info("Looking for user entries.")
        website_entries = []
        for entry in table.find(history=False):
            del entry['id']  # remove the db ids to prevent duplicates
            website_entries.append(entry)
        self.logger.info("Got {} user entries. Saving them...".format(len(website_entries)))
        history_lines = False
        history_entries = []
        # Open the downloaded history file and read every line
        # Only the the price history lines getting processed because we get the gas stations IDs from user input
        self.logger.info("Iterating through archive.")
        try:
            with gzip.open(os.path.join(app_path.get_app_path(), 'temp/history.gz'), 'rb') as infile:
                for line in infile:
                    str_line = str(line)
                    if "COPY gas_station_information_history" in str_line:
                        # Set the variable to True if the right data begins in the dump.
                        # If the variable is True, the following lines gets written into the db.
                        history_lines = True
                    if str_line == "\.":
                        # End of the data write the rest of the data into db and quit
                        table_temp.insert_many(rows=history_entries, chunk_size=1000)
                        history_entries.clear()
                        break
                    if history_lines:
                        if len(str_line) > 0 and "COPY gas_station_information_history" not in str_line:
                            entry = str_line.split(r'\t')[1:6]
                            try:
                                entry_date = datetime.datetime.strptime(entry[4].split('+')[0], "%Y-%m-%d %H:%M:%S")
                                # only add the last x days
                                if (datetime.datetime.now() - entry_date).days <= days:
                                    history_entries.append(dict(
                                        s_id=entry[0],
                                        e5=int(entry[1]),
                                        e10=int(entry[2]),
                                        diesel=int(entry[3]),
                                        date=datetime.datetime.strptime(entry[4].split('+')[0], "%Y-%m-%d %H:%M:%S"),
                                        history=True
                                    ))
                            except:
                                pass
                    # Write entries to DB if the list is too big to prevent a MemoryError
                    if len(history_entries) > 10000:
                        table_temp.insert_many(history_entries, chunk_size=1000)
                        history_entries.clear()
                self.logger.info("Iterations done. Writing list to DB.")
                table_temp.insert_many(history_entries, chunk_size=10000)
                if len(website_entries) > 0:
                    table_temp.insert_many(website_entries, chunk_size=10000)

            # Try to replace the old database with the new 20 times with a pause of 5 sec.
            self.logger.info("Replacing old db with the new one.")
        except OSError:
            self.logger.error(
                "Download is corrupt. Got no valid data from Tankerkoenig. Please check downloadlink in config."
            )
            sys.exit(1)
        except Exception as e:
            self.logger.exception(e)
            sys.exit(1)
        retrys = 0
        while retrys <= 20:
            try:
                db = None
                temp_db = None
                table = None
                table_temp = None
                self.logger.debug("Copy from {} to {}".format(
                    os.path.join(app_path.get_app_path(), "database/history_temp.db"),
                    os.path.join(app_path.get_app_path(), "database/history.db")))
                shutil.copy(os.path.join(app_path.get_app_path(), "database/history_temp.db"),
                            os.path.join(app_path.get_app_path(), "database/history.db"))
                break
            except Exception as e:
                self.logger.error("Db locked, waiting for 10 sec. Round {}/20".format(retrys))
                self.logger.exception(e)
                retrys += 1
                time.sleep(5)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def cheapest(price_list):
    """
    Calculates the cheapest time of a given dict
    :return: Datetime and Price of the cheapest one
    """
    price = None
    date = None
    for key, value in price_list.items():
        if not price or value < price:
            price = value
            date = key
    return [date, price]

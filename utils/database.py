#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import dataset
import datetime
import logging
import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from utils import app_path


class Database:
    """
    Provides a interface for the history data generated from the Tankerkönig history
    https://creativecommons.tankerkoenig.de/#history
    """

    def __init__(self):
        if not os.path.isdir(os.path.join(app_path.get_app_path(), "database")):
            os.mkdir(os.path.join(app_path.get_app_path(), "database"))
        self.db = dataset.connect("sqlite:///" + os.path.join(app_path.get_app_path(), "database/history.db"))
        self.table = self.db['history_data']
        self.logger = logging.getLogger("logger")

    def get_history(self, s_id):
        """
        :param s_id: Gas station ID
        :return: all history data for the gas station as a list
        """
        try:
            entries = []
            for entry in self.table.find(s_id=s_id):
                entries.append(entry)
            self.logger.debug("Got {} history entries for {}".format(len(entries), s_id))
            return entries
        except Exception as e:
            logging.exception(e)

    def add_history(self, data):
        """
        Adds a history entry into the database.
        history=False marks the history as permanent
        :param data: Gas station details
        :return: True or False
        """
        try:
            values = dict()
            values.update(s_id=data['id'], date=datetime.datetime.now(), history=False)
            if data['e5'] or data['e5'] == 0:
                values.update(e5=int(data['e5']*1000))
            else:
                values.update(e5=0)
            if data['e10'] or data['e10'] == 0:
                values.update(e10=int(data['e10']*1000))
            else:
                values.update(e10=0)
            if data['diesel'] or data['diesel'] == 0:
                values.update(diesel=int(data['diesel']*1000))
            else:
                values.update(diesel=0)
            self.table.insert(values)
            return True
        except Exception as e:
            self.logger.exception(e)
            return False

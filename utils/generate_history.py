#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging
import time
import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from logging.handlers import RotatingFileHandler
from utils import history, app_path

# Logging
logger = logging.getLogger("history_logger")
logger.setLevel(logging.DEBUG)
if not os.path.isdir(os.path.join(app_path.get_app_path(), 'log')):
    os.mkdir(os.path.join(app_path.get_app_path(), 'log'))
handler = RotatingFileHandler(os.path.join(app_path.get_app_path(), 'log', 'generate_history.log'),
                              10485760, 10)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s [%(module)-12.12s] [%(levelname)-5.5s]  %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
streamHandler = logging.StreamHandler()
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)


def main():
    """
    Generates history data for 30 days from now
    See utils/history.py
    """
    start = time.time()
    logger.info("Generator started.")
    h = history.History()
    h.re_generate_db(30)
    end = time.time()
    logger.info("Genrator done in {}s.".format(round(end-start, 2)))

if __name__ == "__main__":
    main()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import datetime
import json
import logging
import os
import sys

import dataset

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from config import config
from utils import app_path


class Cache:
    """
    Provides a caching functionallity for gas station details to match the usage requirements from the API provider:
    https://creativecommons.tankerkoenig.de/#usage
    """

    def __init__(self):
        if not os.path.isdir(os.path.join(app_path.get_app_path(), "database")):
            os.mkdir(os.path.join(app_path.get_app_path(), "database"))
        self.db = dataset.connect("sqlite:///" + os.path.join(app_path.get_app_path(), "database/cache.db"))
        self.table = self.db['cache']
        self.logger = logging.getLogger("logger")

    def is_cached(self, s_id):
        """
        Checks if a gas station detail is cached already. If the cached data is older than the config value it deletes
        the data and returns False.
        :param s_id: Gas station ID
        :return: True or False
        """
        try:
            entry = self.table.find_one(s_id=s_id)
            if entry:
                if (datetime.datetime.now() - entry['date']).seconds > config.CACHING_TIME:
                    self.logger.debug("Cache for {} is too old. Deleting it.".format(s_id))
                    self._del_cached(s_id=s_id)
                    return False
                else:
                    return True
            else:
                return False
        except Exception as e:
            self.logger.exception(e)

    def get_cached(self, s_id):
        """
        Returns the cached data if there is one.
        :param s_id: Gas station ID
        :return: Cached details from a gas station in json format
        """
        try:
            if self.is_cached(s_id=s_id):
                self.logger.debug("Returning cached details for station {}.".format(s_id))
                entry = self.table.find_one(s_id=s_id)
                return json.loads(entry['data'])
            else:
                self.logger.debug("Details for station {} are not cached.".format(s_id))
                return False
        except Exception as e:
            logging.exception(e)

    def set_cached(self, data):
        """
        Writes gas station detail data into the database
        :param data: Json response object from the API Class
        :return True if ok False if not
        """
        try:
            if not self.is_cached(data['id']):
                entry = dict()
                entry['data'] = (json.dumps(data))
                entry['date'] = datetime.datetime.now()
                entry['s_id'] = data['id']
                self.table.insert(entry)
                return True
            else:
                self.logger.warn("Data alread in cache: {}".format(data['id']))
                return False
        except Exception as e:
            self.logger.exception(e)
            return False

    def _del_cached(self, s_id):
        """
        Deletes a cached object
        :param s_id:  Gas station ID
        """
        try:
            self.table.delete(s_id=s_id)
        except Exception as e:
            self.logger.exception(e)

    def _clean_cache(self):
        """
        Clean the whole cache
        :return:
        """
        try:
            self.logger.info("Deleting cache")
            self.table.drop()
        except Exception as e:
            self.logger.exception(e)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import unittest
from utils import zip_code


class ZipCode(unittest.TestCase):
    codes = (
        ("86633", True),
        ("85053", True),
        ("85077", True),
        ("63928", True),
        ("63811", True),
        ("97892", True),
        ("97276", True),
        ("21481", True),
        ("25999", True),
        ("21037", True),
        ("34212", True),
        ("69518", True),
        ("32361", True),
        ("04600", True),
        ("04618", True),
        ("01550", True),
        ("80796", True),
        ("1234", False),
        ("00000", False),
        (" ", False),
        ("Straßenname 1 in Ingolstadt", False),
        ("", False),
        ("05234", False),
        ("8973", False),
        ("62980", False),

    )

    def test_zip_codes(self):
        for code, valid in self.codes:
            print("Testing zip {} should be {}".format(code, valid))
            result = zip_code.is_german_zip(code)
            self.assertEqual(result, valid)

    if __name__ == '__main__':
        unittest.main()

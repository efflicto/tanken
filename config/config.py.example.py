# Flask secret key. http://flask.pocoo.org/docs/0.11/quickstart/
# Can be anything for testing...
FLASK_SECRET_KEY = ""

# From https://creativecommons.tankerkoenig.de/
API_KEY = ""

# Google Maps API key from https://developers.google.com/maps/documentation/embed/
# This key is not a must have but the browser console puts out a warning and the number of requests are limited.
# So providing an API key raises the requests per day up to 25000 per day or 100 per sec/user
GMAPS_API_KEY = ""

# Google Analytics Key UA-XXXXXXXX-X can be empty if you don't want to use it
GANALYTICS_KEY = ""

LIST_URL = "https://creativecommons.tankerkoenig.de/json/list.php"
DETAIL_URL = "https://creativecommons.tankerkoenig.de/json/detail.php"
PRICES_URL = "https://creativecommons.tankerkoenig.de/json/prices.php"
HISTORY_URL = "https://creativecommons.tankerkoenig.de/history/history.dump.gz"

CACHING_TIME = 300  # 5 Minutes

# Mailserver settings for contact form
MAIL_SERVER = ""  # Your mail server
MAIL_PORT = 465
MAIL_USE_SSL = True
MAIL_USERNAME = ""  # Your user name
MAIL_PASSWORD = ""  # Your password
MAIL_SENDER = ""  # Sender address
MAIL_RECIPIENT = [  # A list of recipients
    "",
]
